def call(tomcatIp,tomcatUsr,finalWarName){
  
  sshagent(['tom-key-dep']) {
      sh "scp -o StrictHostKeyChecking=no target/${finalWarName}.war ${tomcatUsr}@${tomcatIp}:/opt/tomcat-9/webapps"
      sh """
          ssh ${tomcatUsr}@${tomcatIp} /opt/tomcat-9/bin/shutdown.sh
          ssh ${tomcatUsr}@${tomcatIp} /opt/tomcat-9/bin/startup.sh
      """
  }
}
